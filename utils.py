#!/usr/bin/env python3.6

def check_for_tag(project, tag):
    """ Check if tag exists in project """
    return tag in [ tag.get_id() for tag in project.tags.list() ]

def check_for_branch(project, branch):
    """ Check if tag exists in project """
    return branch in [ branch.get_id() for branch in project.branches.list() ]

def get_branch_refs_for_tag(project, tag):
    """:returns list of branch references for tag in project"""
    return [ branch['name'] for branch in
             project.commits.get(project.tags.get(tag).attributes['target']).refs('branch') ]

