#!/usr/bin/env python3.6

from flask import request, Flask
from celery import Celery
import urllib3
import smtplib
import os
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

app = Flask(__name__)

app.config['CELERY_BROKER_URL'] = 'amqp://localhost//'

def msg_tag(tag):
    subject = "New release was tagged"
    html = f"""\
     <html>
       <body>
         <p>Hi,</p>
         <p>This mail is to inform you that release <b>{tag.split("/")[-1]}</b> was just tagged</p>
         <p>This is an automatically generated email. Please do not reply.</p>
       </body>
     </html>
     """
    return (html, subject)
    
def msg_file_added(file_added):
    subject = "New file/s added"
    html = f"""\
     <html>
       <body>
         <p>Hi,</p>
         <p>This mail is to inform you that new file/s were added: <b>{file_added}</b></p>
         <p>This is an automatically generated email. Please do not reply.</p>
       </body>
     </html>
     """
    return (html, subject)

def sendmail(html, subject):

    if 'SENDER_EMAIL' not in os.environ:
        logger.critical("Can't find environment variable SENDER_EMAIL!")
        sys.exit(10)
    elif 'RECEIVER_EMAIL' not in os.environ:
        logger.critical("Can't find environment variable RECEIVER_EMAIL!")
        sys.exit(10)
    elif 'SMTP_SERVER' not in os.environ:
        logger.critical("Can't find environment variable SMTP_SERVER!")
        sys.exit(10) 
    elif 'SMTP_PORT' not in os.environ:
        logger.critical("Can't find environment variable SMTP_PORT!")
        sys.exit(10)         
    else:
        sender_email = os.environ['SENDER_EMAIL']
        receiver_email = os.environ['RECEIVER_EMAIL']
        smtp_server = os.environ['SMTP_SERVER']
        smtp_port = os.environ['SMTP_PORT']
        
        #sender_email = "sender@mail.com"
        #receiver_email = "test@mail.com"
        #password = "pass"

        message = MIMEMultipart("alternative")
        message["Subject"] = subject
        message["From"] = sender_email
        message["To"] = receiver_email


        # Turn these into html MIMEText objects
        part1 = MIMEText(html, "html")


        # Add HTML parts to MIMEMultipart message
        message.attach(part1)


        # Create connection with server and send email
        with smtplib.SMTP(smtp_server, smtp_port) as server:
          #server.login(sender_email, password)
          server.sendmail(
              sender_email, receiver_email, message.as_string()
          )


def make_celery(app):
    celery_object = Celery(
        app.import_name,
        broker=app.config['CELERY_BROKER_URL']
    )
    celery_object.conf.update(app.config)

    class ContextTask(celery_object.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery_object.Task = ContextTask
    return celery_object


celery = make_celery(app)

# Implementation of POST method
@app.route('/tag-push', methods=['POST'])
def api_gl_tag():
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
    if request.headers['Content-Type'] == 'application/json':
        trigger_at_tag_push.delay(request.json)
    # we must return a string
    return "Tag push event received"


@celery.task(name='gitlab_webhook.api_gl_tag')
def trigger_at_tag_push(event_dictionary):
    """Implement behaviour when the hook is triggered by GitLab"""
    app.logger.info("Executed via task queue")
    app.logger.info('Received gitlab payload: %s', event_dictionary)
    event_kind = event_dictionary['object_kind']
    tag = event_dictionary['ref']
    file_added = [ commit['added'] for commit in event_dictionary['commits'] if commit['added'] ]
    #tag2 = tag.split("/")[-1]
    #app.logger.info(event_kind)
    #app.logger.info(('check variable: %s', file_added))
    
    tag_create = False
    
    if event_kind == 'tag_push':
      try:
        int(event_dictionary['after'])
      except ValueError:
        tag_create = True
      if tag_create:
        #print ("new tag added %s" % (tag).split("/")[-1])
        #sendmail()
        print (msg_tag(tag))
        html, subject = msg_tag(tag)
        sendmail(html, subject)
        
        
    if event_kind == 'push':
        if file_added:
            print(msg_file_added(file_added))
            html, subject = msg_file_added(file_added)
            sendmail(html, subject)
        
# Just a description of web-service
@app.route('/')
def api_root():
    return "This is a Flask app for webhook purpose which listens to push tags events"


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
