#!/usr/bin/env python3.6

from flask import request, Flask
from celery import Celery
import urllib3

app = Flask(__name__)

app.config['CELERY_BROKER_URL'] = 'amqp://localhost//'


def make_celery(app):
    celery_object = Celery(
        app.import_name,
        broker=app.config['CELERY_BROKER_URL']
    )
    celery_object.conf.update(app.config)

    class ContextTask(celery_object.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery_object.Task = ContextTask
    return celery_object


celery = make_celery(app)

# Implementation of POST method
@app.route('/tag-push', methods=['POST'])
def api_gl_mr():
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
    if request.headers['Content-Type'] == 'application/json':
        trigger_at_tag_push.delay(request.json)
    # we must return a string
    return "Tag push event received"


@celery.task(name='gitlab_webhook.api_gl_mr')
def trigger_at_tag_push(event_dictionary):
    """Implement behaviour when the hook is triggered by GitLab"""
    app.logger.info("Executed via task queue")
    app.logger.info('Received gitlab payload: %s', event_dictionary)


# Just a description of web-service
@app.route('/')
def api_root():
    return "This is a Flask app for webhook purpose which listens to merge requests"


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
