# Flask GitLab Webhook Service

## Overview

This is a Flask-based web application designed to listen for GitLab webhook events, specifically `tag_push` and `push` for a configured project. Upon receiving these events, the application processes the data and sends email notifications to the configured recipient using Celery for asynchronous task handling.

It is configured to run inside a docker container

---

## Features

- Listens to `tag_push` and `push` events from GitLab for a configured project.
- Sends email notifications when a new tag is created or files are added.
- Utilizes Celery for asynchronous task processing.

---

## Requirements

### System Requirements
- docker daemon running on the host

### Python Dependencies
- Flask
- Celery
- smtplib (built-in with Python)
- urllib3
- email (built-in with Python)

They are configured in the Dockerfile
---

## Environment Variables

The application requires the following environment variables to be set:

| Variable        | Description                     |
|-----------------|---------------------------------|
| `SENDER_EMAIL`  | The email address to send from  |
| `RECEIVER_EMAIL`| The email address to send to    |
| `SMTP_SERVER`   | The SMTP server address         |
| `SMTP_PORT`     | The SMTP server port            |

---

## Usage

### 1. Configure the webhook in Gitlab:
To setup the webhook in gitlab:

project - settings - webhooks

url: http://app-server:8089/tag-push

trigger: push events


### 2. Configure environment variables:
Edit the script:

```bash
start.sh
```

### 3. Start the Flask application:
Run the script to build and run the application with docker:

```bash
docker-gitlab-tag-hook
```

The application will run on `http://0.0.0.0:8089` by default.

