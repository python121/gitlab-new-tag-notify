import gitlab
import os, sys
import logging

logger = logging.getLogger('hooks_add')
logger.setLevel(logging.DEBUG)


def authenticate():
    if 'GITLAB_URL' not in os.environ:
        logger.critical("Can't find environment variable GITLAB_URL!")
        sys.exit(10)
    elif 'GITLAB_TOKEN' not in os.environ:
        logger.critical("Can't find environment variable GITLAB_TOKEN!")
        sys.exit(10)
    else:
        gitlab_url = os.environ['GITLAB_URL']
        token = os.environ['GITLAB_TOKEN']
    gl = gitlab.Gitlab(gitlab_url, private_token=token, ssl_verify=False, per_page=100)
    return gl


def get_projects_in_groups(groups, gl):
    """Returns a list of projects in groups with names specified in `groups` list or tuple"""
    return [ gl.projects.get(project.id) for group in gl.groups.list() for project in
             group.projects.list() if group.full_path in groups ]


def add_hook_to_project(project, url, enable_events):
    """Add hook to project if not already defined"""
    if url not in [ hook.url for hook in project.hooks.list() ]:
        hook_dict = { 'url': url }
        for event in enable_events:
            hook_dict.update({event: 1})
        if 'push_events' not in enable_events:
            hook_dict.update({'push_events': 0})
        project.hooks.create(hook_dict)

gl = authenticate()
groups = ['ansible','kubernetes/helm']

hook_url = os.getenv('HOOK_URL')
if not hook_url:
    logger.critical("Can't find environment variable HOOK_URL!")
    sys.exit(10)

projects = get_projects_in_groups(groups, gl)
for project in projects:
    add_hook_to_project(project, hook_url, ('tag_push_events', 'merge_requests_events'))
