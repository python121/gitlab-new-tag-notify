FROM python:3.6.9
MAINTAINER name@mail.com
ARG http_proxy
ARG https_proxy
EXPOSE 8080
RUN apt-get update
RUN wget -O - "https://packagecloud.io/rabbitmq/rabbitmq-server/gpgkey" | apt-key add -
## Install RabbitMQ signing key
RUN curl -fsSL https://github.com/rabbitmq/signing-keys/releases/download/2.0/rabbitmq-release-signing-key.asc | apt-key add -

## Install apt HTTPS transport
RUN apt-get install apt-transport-https

## Add Bintray repositories that provision latest RabbitMQ and Erlang 21.x releases
ADD bintray.rabbitmq.list /etc/apt/sources.list.d/bintray.rabbitmq.list

## Update package indices
RUN apt-get update -y

## Install rabbitmq-server and its dependencies
RUN apt-get install rabbitmq-server -y --fix-missing
RUN pip install celery flask python-gitlab requests gunicorn
RUN pip install PyYAML

ADD gitlab_tag_push_webhook_v2.py /opt
ADD start.sh /opt

CMD /opt/start.sh

RUN useradd celery
