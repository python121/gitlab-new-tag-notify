#!/usr/bin/env bash

#export variables
export SENDER_EMAIL="sender@mail.com"
export RECEIVER_EMAIL="dest@mail.com"
export SMTP_SERVER="10.1.1.1"
export SMTP_PORT=25

cd /opt
pgrep rabbitmq 2>&1 1>/dev/null || (nohup rabbitmq-server &)
celery -A gitlab_tag_push_webhook_v2.celery worker --loglevel=info --uid=celery &
gunicorn -w 4 -b 0.0.0.0:8080 gitlab_tag_push_webhook_v2:app
